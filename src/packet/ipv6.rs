use super::types::{ Protocol, Packet };

use std::net::Ipv6Addr;

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Ipv6Packet<'v6payload> {
    pub source_ip: Ipv6Addr,
    pub destination_ip: Ipv6Addr,
    pub protocol: Protocol,
    pub payload: &'v6payload [u8],
}

impl<'raw> Ipv6Packet<'raw> {
    pub fn new(raw: &'raw [u8]) -> Self {
        Ipv6Packet {
            source_ip: Ipv6Addr::new(
               (raw[8]  as u16) <<8 | raw[9]  as u16,
               (raw[10] as u16) <<8 | raw[11] as u16,
               (raw[12] as u16) <<8 | raw[13] as u16,
               (raw[14] as u16) <<8 | raw[15] as u16,
               (raw[16] as u16) <<8 | raw[17] as u16,
               (raw[18] as u16) <<8 | raw[19] as u16,
               (raw[20] as u16) <<8 | raw[21] as u16,
               (raw[22] as u16) <<8 | raw[23] as u16,
            ),
            destination_ip: Ipv6Addr::new(
               (raw[24] as u16) <<8 | raw[25] as u16,
               (raw[26] as u16) <<8 | raw[27] as u16,
               (raw[28] as u16) <<8 | raw[29] as u16,
               (raw[30] as u16) <<8 | raw[31] as u16,
               (raw[32] as u16) <<8 | raw[33] as u16,
               (raw[34] as u16) <<8 | raw[35] as u16,
               (raw[36] as u16) <<8 | raw[37] as u16,
               (raw[38] as u16) <<8 | raw[39] as u16,
            ),
            protocol: Protocol::from(&raw[6]), 
            payload: &raw[40..],
        }
    }
}

impl Packet for Ipv6Packet<'_> {
    fn get_payload(&self) -> &[u8] {
        self.payload
    }
}
