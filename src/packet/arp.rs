use super::mac_addr::{ MacAddr, MacAddrOctets };

#[derive(Debug, PartialEq, Clone)]
pub struct ArpPacket {
    sha: MacAddr,
    tha: MacAddr,
}

impl ArpPacket {
    #[allow(dead_code)]
    fn new(raw: &[u8]) -> Self {
        ArpPacket {
            sha: MacAddr::Octets(MacAddrOctets::from(&raw[14..18])),
            tha: MacAddr::Octets(MacAddrOctets::from(&raw[24..28])),
        }
    }
}


