use super::types::Packet;

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct UdpPacket<'udppayload> {
    pub source_port: u16,
    pub destination_port: u16,
    pub payload: &'udppayload [u8],
}

impl<'raw> UdpPacket<'raw> {
    pub fn new(raw: &'raw [u8]) -> Self {
        UdpPacket {
            source_port: ((raw[0] as u16) << 8) | raw[1] as u16,
            destination_port: ((raw[2] as u16) << 8) | raw[3] as u16,
            payload: &raw[9..],
        }
    }
}

impl Packet for UdpPacket<'_> {
    fn get_payload(&self) -> &[u8] {
        self.payload
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_udp_new() {
        let packet = [ 255, 255, 128, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ];
        let udp_packet = UdpPacket::new(&packet);

        assert_eq!(udp_packet.source_port, 65535);
        assert_eq!(udp_packet.destination_port, 32896);
    }
}
