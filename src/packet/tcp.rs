use super::types::Packet;

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct TcpPacket<'tcppayload> {
    pub source_port: u16,
    pub destination_port: u16,
    pub flags: &'tcppayload u8,
    pub payload: &'tcppayload [u8],
}

impl<'raw> TcpPacket<'raw> {
    pub fn new(raw: &'raw [u8]) -> Self {
        TcpPacket {
            source_port: ((raw[0] as u16) << 8) | raw[1] as u16,
            destination_port: ((raw[2] as u16) << 8) | raw[3] as u16,
            flags: &raw[13],
            payload: if raw.len() <= 20 {
                &[]
            } else {
                &raw[24..]
            },
        }
    }
}

impl Packet for TcpPacket<'_> {
    fn get_payload(&self) -> &[u8] {
        self.payload
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_tcp_new() {
        let packet = [ 255, 255, 128, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ];
        let udp_packet = TcpPacket::new(&packet);

        assert_eq!(udp_packet.source_port, 65535);
        assert_eq!(udp_packet.destination_port, 32896);
    }
}
