use super::tcp::TcpPacket;
use super::udp::UdpPacket;

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Protocol {
    Udp,
    Tcp,

    Unknown
}

impl From<&u8> for Protocol {
    fn from(protocol: &u8) -> Self {
        match protocol {
            0x06 => Protocol::Tcp,
            0x11 => Protocol::Udp,

            _    => Protocol::Unknown
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum EtherType {
    Ipv4,
    Ipv6,
    Arp,

    Unknown
}

impl From<&[u8]> for EtherType {
    fn from(ethertype: &[u8]) -> Self {
        match ((ethertype[0] as u16) << 8) | ethertype[1] as u16 {
            0x0800 => EtherType::Ipv4,
            0x086DD => EtherType::Ipv6,
            0x0806 => EtherType::Arp,

            _ => EtherType::Unknown
        }
    }
}

pub trait Packet {
    fn get_payload(&self) -> &[u8];
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum TransportPacket<'payload> {
    Tcp(TcpPacket<'payload>),
    Udp(UdpPacket<'payload>),
    Unknown,
}

impl TransportPacket<'_> {
    pub fn get_source_port(&self) -> Result<u16, &str> {
        match self {
            TransportPacket::Tcp(packet) => Ok(packet.source_port),
            TransportPacket::Udp(packet) => Ok(packet.source_port),
            _                            => Err("Not a udp or tcp packet"),
        }
    }

    pub fn get_destination_port(&self) -> Result<u16, &str> {
        match self {
            TransportPacket::Tcp(packet) => Ok(packet.destination_port),
            TransportPacket::Udp(packet) => Ok(packet.destination_port),
            _                            => Err("Not a udp or tcp packet"),
        }
    }
}

impl Packet for TransportPacket<'_> {
    fn get_payload(&self) -> &[u8] {
        match self {
            TransportPacket::Tcp(packet) => packet.get_payload(),
            TransportPacket::Udp(packet) => packet.get_payload(),
            _                    => unimplemented!("not handling currently: {:?}", self),
        }
    }
}

