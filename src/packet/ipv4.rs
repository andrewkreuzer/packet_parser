use super::types::{ Protocol, Packet };

use std::net::Ipv4Addr;

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Ipv4Packet<'v4payload> {
    pub source_ip: Ipv4Addr,
    pub destination_ip: Ipv4Addr,
    pub protocol: Protocol,
    pub payload: &'v4payload [u8],
}

impl<'raw> Ipv4Packet<'raw> {
    pub fn new(raw: &'raw [u8]) -> Self {
        Ipv4Packet {
            source_ip: Ipv4Addr::new(raw[12], raw[13], raw[14], raw[15]),
            destination_ip: Ipv4Addr::new(raw[16], raw[17], raw[18], raw[19]),
            protocol: Protocol::from(&raw[9]), 
            payload: &raw[20..],
        }
    }
}

impl Packet for Ipv4Packet<'_> {
    fn get_payload(&self) -> &[u8] {
        self.payload
    }
}


