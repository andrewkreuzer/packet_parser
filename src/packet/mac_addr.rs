use std::fmt;
use std::str::FromStr;

#[derive(Debug, PartialEq, Clone)]
pub enum MacAddr {
    String(MacAddrString),
    Octets(MacAddrOctets)
}

impl MacAddr {
    pub fn is_mac_string(&self) -> bool {
        matches!(self, MacAddr::String(_))
    }

    pub fn is_octect_array(&self) -> bool {
        matches!(self, MacAddr::Octets(_))
    }
}

impl fmt::Display for MacAddr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let string = match self {
            MacAddr::Octets(octets) => MacAddrString::from(octets).inner,
            MacAddr::String(string) => string.inner.to_owned()
        };
        write!(f, "{}", string)
    }
}

#[derive(Debug, Clone, Copy)]
pub struct MacAddrOctets {
    inner: [u8; 6],
}

impl MacAddrOctets {
    pub fn new(a: u8, b: u8, c: u8, d: u8, e: u8, f: u8) -> Self {
        MacAddrOctets {
            inner: [a, b, c, d, e, f],
        }
    }
}

impl From<&[u8]> for MacAddrOctets {
    fn from(octets: &[u8]) -> Self {
        MacAddrOctets::new(octets[0], octets[1], octets[2], octets[3], octets[4], octets[5])
    }
}

impl PartialEq for MacAddrOctets {
    fn eq(&self, other: &MacAddrOctets) -> bool {
        self.inner == other.inner
    }
}

#[derive(Debug, Clone)]
pub struct MacAddrString {
    pub inner: String,
}

impl From<&[u8; 6]> for MacAddrString {
    fn from(raw: &[u8; 6]) -> MacAddrString {
        raw
            .iter()
            .map(|part| format!("{:X?}", part))
            .collect::<Vec<String>>()
            .join(":")
            .into()
    }
}

impl From<&MacAddrOctets> for MacAddrString {
    fn from(address: &MacAddrOctets) -> MacAddrString {
        address
            .inner
            .iter()
            .map(|part| format!("{:X?}", part))
            .collect::<Vec<String>>()
            .join(":")
            .into()
    }
}

// we assume the provided string is correct...
// better to use FromStr with .parse() it will 
// atleast check for correct length
impl From<String> for MacAddrString {
    fn from(address: String) -> MacAddrString {
        MacAddrString {
            inner: address
        }
    }
}

impl From<&str> for MacAddrString {
    fn from(address: &str) -> MacAddrString {
        MacAddrString {
            inner: address.to_owned()
        }
    }
}

impl FromStr for MacAddrString {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.split(":").count() == 6 {
            Ok(MacAddrString { inner: s.to_owned() })
        } else {
            Err("The string was not a valid MacAddr".into())
        }
    }
}

impl PartialEq for MacAddrString {
    fn eq(&self, other: &MacAddrString) -> bool {
        self.inner == other.inner
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_mac_addr_string_from_u8_array() {
        let mac_addr = MacAddrString::from(&[255,2,3,4,5,6]);
        assert_eq!(mac_addr, "FF:2:3:4:5:6".parse().unwrap());
    }
}
