use super::types::{ EtherType, Packet, Protocol };
use super::mac_addr::{ MacAddr, MacAddrOctets };
use super::ipv4::Ipv4Packet;
use super::ipv6::Ipv6Packet;

use std::net::IpAddr;

pub struct EthernetPacket<'epayload> {
    pub destination_mac: MacAddr,
    pub source_mac: MacAddr,
    pub ethertype: EtherType,
    pub payload: &'epayload [u8],
}

impl<'raw> EthernetPacket<'raw> {
    pub fn new(raw: &'raw [u8]) -> Self {
        EthernetPacket {
            destination_mac: MacAddr::Octets(MacAddrOctets::from(&raw[..6])),
            source_mac: MacAddr::Octets(MacAddrOctets::from(&raw[6..12])),
            ethertype: EtherType::from(&raw[12..14]),
            payload: &raw[14..],
        }
    }

    pub fn get_source_mac(&self) -> MacAddr {
        self.source_mac.to_owned()
    }

    pub fn get_ethertype(&self) -> EtherType {
        self.ethertype.to_owned()
    }
}

impl Packet for EthernetPacket<'_> {
    fn get_payload(&self) -> &[u8] {
        self.payload
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum EtherProtocol<'payload> {
    V4(Ipv4Packet<'payload>),
    V6(Ipv6Packet<'payload>),
    Arp(),
    Unknown
}

impl EtherProtocol<'_> {
    pub fn get_source_ip(&self) -> Result<IpAddr, &str> {
        match self {
            EtherProtocol::V4(packet) => Ok(IpAddr::V4(packet.source_ip)),
            EtherProtocol::V6(packet) => Ok(IpAddr::V6(packet.source_ip)),
            _                         => Err("Not an ipv4 or v6 packet"),
        }
    }

    pub fn get_destination_ip(&self) -> Result<IpAddr, &str> {
        match self {
            EtherProtocol::V4(packet) => Ok(IpAddr::V4(packet.destination_ip)),
            EtherProtocol::V6(packet) => Ok(IpAddr::V6(packet.destination_ip)),
            _                         => Err("Not an ipv4 or v6 packet"),
        }
    }

    pub fn get_protocol(&self) -> Result<Protocol, &str> {
        match self {
            EtherProtocol::V4(packet) => Ok(packet.protocol),
            EtherProtocol::V6(packet) => Ok(packet.protocol),
            _                         => Err("Not an ipv4 or v6 packet"),
        }
    }

    pub fn get_payload(&self) -> Result<&[u8], &str> {
        match self {
            EtherProtocol::V4(packet) => Ok(packet.payload),
            EtherProtocol::V6(packet) => Ok(packet.payload),
            _                         => Err("Not an ipv4 or v6 packet"),
        }
    }
}


